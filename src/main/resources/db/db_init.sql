create table Partner
(
    id        BIGSERIAL PRIMARY KEY,
    name      VARCHAR(255)                      NOT NULL,
    image_url VARCHAR DEFAULT 'PHOTO_NOT_FOUND' NOT NULL,
    last_update TIMESTAMP DEFAULT(now())
);

create table Action
(
    id            BIGSERIAL PRIMARY KEY,
    name          VARCHAR(255),
    status        VARCHAR(255),
    description   VARCHAR(20000),
    discount      VARCHAR(255),
    species       VARCHAR(255),
    promocode     VARCHAR(255),
    frameset_link VARCHAR(255),
    goto_link     VARCHAR(255),
    short_name    VARCHAR(255),
    date_start    TIMESTAMP   ,
    date_end      TIMESTAMP   ,
    image_url     VARCHAR(255),
    partner_id    BIGINT,
    last_update TIMESTAMP DEFAULT(now())
);

ALTER TABLE Action
    ADD CONSTRAINT partner_id_action_id_fk FOREIGN KEY (partner_id) REFERENCES Partner (id) ON DELETE CASCADE


CREATE TABLE Region
(
    id        bigserial,
    name      VARCHAR(5) NOT NULL,
    action_id bigint
);

alter table region
    add constraint region_action_id foreign key (action_id) references action (id);