package com.actionservice.model;

import lombok.Data;

@Data
public class ErrorResponse {

    private String message;
}
