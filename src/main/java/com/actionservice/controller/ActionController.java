package com.actionservice.controller;

import com.actionservice.model.Action;
import com.actionservice.model.Actions;
import com.actionservice.repository.ActionDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/actions")
@RequiredArgsConstructor
public class ActionController {

    private final ActionDao actionDao;

    @GetMapping
    public Actions findAll() {
        log.info("ActionController findAll");
        Actions actions = new Actions();
        actions.setActions(actionDao.findAll());
        return actions;
    }

    @GetMapping("/{id}")
    public Action findById(@PathVariable Long id){
        log.info("ActionController findById - {}", id);
        return actionDao.findById(id);
    }

}
