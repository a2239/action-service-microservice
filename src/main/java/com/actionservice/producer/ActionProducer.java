package com.actionservice.producer;

import com.actionservice.client.AdmitadContentClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@RequiredArgsConstructor
public class ActionProducer {

    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final AdmitadContentClient admitadContentClient;

    public void actionProduce() {
        log.info("ActionProducer actionProduce begin start message");

        log.info("ActionProducer actionProduce, message have sent actions - {}", "actions");
    }
}
