package com.actionservice.config.kafka;

import com.actionservice.model.Partners;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConsumerConfiguration {
    @Value(value = "localhost:29092")
    private String bootstrapAddress;

    @Bean
    public ConsumerFactory<String, Partners> customerConsumer() {
        Map<String, Object> customerConfigMap = new HashMap<>();
        customerConfigMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        customerConfigMap.put(ConsumerConfig.GROUP_ID_CONFIG, "group_json");
        customerConfigMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        customerConfigMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        customerConfigMap.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        return new DefaultKafkaConsumerFactory<>(customerConfigMap, new StringDeserializer(),
                new JsonDeserializer<>(Partners.class, false));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Partners> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, Partners> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(customerConsumer());
        return factory;
    }
}
