package com.actionservice.consumer;

import com.actionservice.client.AdmitadContentClient;
import com.actionservice.model.Action;
import com.actionservice.model.Partners;
import com.actionservice.repository.ActionDaoImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class PartnerConsumer {

    private final ActionDaoImpl actionDao;
    private final AdmitadContentClient admitadContentClient;

    @KafkaListener(topics = "partners-topic")
    public void partnerConsume(Partners partners) {
        log.info("PartnerConsumer partnerConsume, have got message, partners - {}", partners.getPartners());
        List<Action> actions = partners.getPartners()
                .stream()
                .flatMap(partner ->
                        admitadContentClient.actionsFromSite(partner.getId()).getActions()
                                .stream())
                .collect(Collectors.toList());
        actionDao.saveAll(actions);
    }
}
