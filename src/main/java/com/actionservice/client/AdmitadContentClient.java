package com.actionservice.client;

import com.actionservice.model.Actions;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "admitadClient", url = "https://api.admitad.com")
public interface AdmitadContentClient {

    @GetMapping(value = "/coupons/website/${websiteId}/?campaign={campaignId}&limit=${limit}")
    Actions actionsFromSite(@PathVariable Long campaignId);

}