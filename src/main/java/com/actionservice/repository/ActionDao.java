package com.actionservice.repository;

import com.actionservice.model.Action;

import java.util.List;

public interface ActionDao {

    List<Action> findAll();

    List<Action> saveAll(List<Action> actions);

    Action save(Action action);

    Action findById(Long id);

}
