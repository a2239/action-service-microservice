package com.actionservice.repository;

import com.actionservice.model.Action;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepository  extends JpaRepository<Action, Long> {
}
