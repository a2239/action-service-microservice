package com.actionservice.repository;

import com.actionservice.model.Action;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class ActionDaoImpl implements ActionDao {

    private final ActionRepository actionRepository;

    @Override
    public List<Action> findAll() {
        log.info("ActionDaoImpl findAll");
        return actionRepository.findAll();
    }

    @Override
    public List<Action> saveAll(List<Action> actions) {
        log.info("ActionDaoImpl saveAll - {}", actions);
        return actionRepository.saveAll(actions);
    }

    @Override
    public Action save(Action action) {
        log.info("ActionDaoImpl save - {}", action);
        return actionRepository.save(action);
    }

    @Override
    public Action findById(Long id) {
        log.info("ActionDaoImpl findById - {}", id);
        return actionRepository.findById(id)
                .orElseThrow();
    }
}
